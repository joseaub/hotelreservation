from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from .models import Customer
import datetime


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')
        customers = serializers.PrimaryKeyRelatedField(queryset=Customer.objects.all())


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = ('user', 'id', 'name', 'address', 'phone')


class CustomerCreateSerializer(serializers.Serializer):
    customerName = serializers.CharField()
    customerAddress = serializers.CharField()

    # def validate_name(self, value):
    #     print(value)
    #     raise serializers.ValidationError("este es un error")
    #
    # def validate_address(self, value):
    #     return value

