from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics, status, mixins
from .serializers import (UserSerializer, GroupSerializer,
    CustomerSerializer, CustomerCreateSerializer)
from .models import Customer
from rest_framework.response import Response


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.all()


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows customers to be viewed or edited.
    """
    serializer_class = CustomerSerializer

    def get_queryset(self):
        return Customer.objects.all()

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        # data1 = request.query_params
        # print(data1.get('data'))

        # data2 = request.data
        # print(data2)
        queryset = self.get_queryset()
        serializer = CustomerSerializer(queryset, context={'request': request}, many=True).data
        # print(serializer)
        return Response(serializer)

    def create(self, request):
        serializer = CustomerSerializer(data=request.data, context={'request': request})
        valid_serializer = CustomerCreateSerializer(data=request.data)

        if valid_serializer.is_valid():
            # valid_serializer.save()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def retrieve(self, request, pk=None):
        queryset = Customer.objects.filter(pk=pk)[0]
        serializer = CustomerSerializer(queryset).data
        # print(serializer)
        return Response(serializer)

    def update(self, request, pk, format=None):
        queryset = Customer.objects.filter(pk=pk)[0]
        serializer = CustomerSerializer(queryset, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def partial_update(self, request, pk=None):
    #     pass

    def destroy(self, request, pk=None):
        queryset = Customer.objects.filter(pk=pk)[0]
        queryset.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
