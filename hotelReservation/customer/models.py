from django.db import models
from django.utils import timezone
from datetime import date
from django.conf import settings
from django.contrib.auth.models import User


class Customer(models.Model):
    user = models.ForeignKey('auth.User', related_name='customers', on_delete=models.CASCADE)
    name = models.CharField(max_length=64, blank=False, null=False)
    phone = models.CharField(max_length=13, blank=True, null=True)
    address = models.CharField(max_length=256, blank=True, null=True)
