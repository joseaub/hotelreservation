from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, generics, status, mixins
from hotelReservation.room.serializers import RoomSerializer, RoomCreateSerializer
from .models import Room
from rest_framework.response import Response
from rest_framework.request import Request


class RoomViewSet(viewsets.ModelViewSet):
    serializer_class = RoomSerializer

    def get_queryset(self):
        return Room.objects.all()

    def create(self, request):
        valid_serializer = RoomCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.save(request.data)
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
