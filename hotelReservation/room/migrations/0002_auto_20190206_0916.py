# Generated by Django 2.1.5 on 2019-02-06 09:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('room', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='room',
            old_name='bedQuantity',
            new_name='bed_quantity',
        ),
        migrations.RenameField(
            model_name='room',
            old_name='maxOccupation',
            new_name='max_occupation',
        ),
    ]
