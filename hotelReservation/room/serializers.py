from rest_framework import serializers
from .models import Room, Hotel


class RoomSerializer(serializers.Serializer):
    max_occupation = serializers.IntegerField()
    bed_quantity = serializers.IntegerField()
    price = serializers.IntegerField()
    hotel = serializers.CharField()


class RoomCreateSerializer(serializers.Serializer):
    number = serializers.IntegerField()
    hotel = serializers.IntegerField()
    price = serializers.IntegerField()
    max_occupation = serializers.IntegerField()
    bed_quantity = serializers.IntegerField()

    def validate_hotel(self, value):
        hotel = value
        hotel_in_db = Hotel.objects.filter(pk=hotel)
        if not hotel_in_db.exists():
            raise serializers.ValidationError('Unknown hotel')
        return value

    def save(self, validated_data):
        hotel = validated_data.get("hotel")
        price = validated_data.get("price")
        number = validated_data.get("number")
        rooms_in_db = Room.objects.filter(number=number, hotel_id=hotel).count()
        max_occupation = validated_data.get("max_occupation")
        bed_quantity = validated_data.get("bed_quantity")
        if rooms_in_db > 0:
            raise serializers.ValidationError('This room already exists')
        instances = Room(number=number, hotel_id=hotel, max_occupation=max_occupation, bed_quantity=bed_quantity, price=price)
        instances.save()
        return True
