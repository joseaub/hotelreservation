from django.db import models
from hotelReservation.hotel.models import Hotel

class Room(models.Model):
    hotel = models.ForeignKey(Hotel, blank=True, on_delete=models.CASCADE)
    number = models.IntegerField(blank=False, null=False)
    max_occupation = models.IntegerField(blank=True, null=False)
    bed_quantity = models.IntegerField(blank=True, null=False)
    price = models.IntegerField(blank=True, null=False)

    # def __str__(self):
    #     return self.number


class Service(models.Model):
    serviceName = models.CharField(max_length=64, blank=False, null=False)

    # def __str__(self):
    #     return self.serviceName


class RoomService(models.Model):
    room = models.ForeignKey('Room', blank=False, on_delete=models.DO_NOTHING)
    service = models.ForeignKey('Service', blank=False, on_delete=models.DO_NOTHING)