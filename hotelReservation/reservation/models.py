from django.db import models
from hotelReservation.room.models import Room


class Reservation(models.Model):
    total = models.IntegerField(blank=False, null=False)
    start = models.DateField(auto_now=False, auto_now_add=False)
    end = models.DateField(auto_now=False, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=True)
    # customer = models.ForeignKey('Customer', blank=True, null=True, on_delete=models.DO_NOTHING)
    # rooms = models.ForeignKey('Room', related_name='rooms', blank=False, null=False, on_delete=models.DO_NOTHING)

    # def __str__(self):
    #     return self.start, self.end


class RoomReservation(models.Model):
    occupation = models.IntegerField(blank=False, null=False)
    room = models.ForeignKey(Room, blank=False, on_delete=models.DO_NOTHING)
    reservation = models.ForeignKey('Reservation', blank=False, on_delete=models.DO_NOTHING)

    # def __str__(self):
    #     return self.occupation
