from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, generics, status, mixins
from .serializers import ReservationSerializer, ReservationCreateSerializer, ListReservationSerializer
from .models import Reservation
from rest_framework.response import Response
from rest_framework.request import Request


class ReservationViewSet(viewsets.ModelViewSet):
    serializer_class = ReservationSerializer

    def get_queryset(self):
        return Reservation.objects.all()

    def list(self, request):
        serializer = ListReservationSerializer()
        data = serializer.get_room_list()
        return Response(data)

    def create(self, request):
        valid_serializer = ReservationCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.save()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
