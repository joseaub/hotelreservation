from rest_framework import serializers
from .models import Reservation, RoomReservation
from hotelReservation.room.models import Room
from hotelReservation.hotel.models import Hotel
import datetime


class RoomSerializer(serializers.Serializer):
    number = serializers.IntegerField()


class ReservationSerializer(serializers.Serializer):
    start = serializers.DateField()
    end = serializers.DateField()
    total = serializers.IntegerField()
    hotel = serializers.SerializerMethodField()
    beds = serializers.SerializerMethodField()
    room_list = serializers.SerializerMethodField()

    def get_room_list(self, object):
        rooms_reservation = list(
            RoomReservation.objects.filter(reservation_id=object.pk).values_list('room_id', flat=True))
        rooms = Room.objects.filter(id__in=rooms_reservation)
        return RoomSerializer(rooms, many=True).data

    def get_hotel(self, object):
        room_reservation = RoomReservation.objects.filter(reservation_id=object.pk).first()
        hotel_id = Room.objects.filter(id=room_reservation.room_id).first()
        hotel = Hotel.objects.filter(id=hotel_id.pk).first()
        return hotel.name

    def get_beds(self, object):
        rooms_reservation = list(
            RoomReservation.objects.filter(reservation_id=object.pk).values_list('room_id', flat=True))
        rooms = Room.objects.filter(id__in=rooms_reservation)
        beds = 0
        for room in rooms:
            beds = room.bed_quantity + beds
        return beds


class ListReservationSerializer(serializers.Serializer):
    def get_room_list(self):
        reservations = Reservation.objects.all()
        return ReservationSerializer(reservations, many=True).data


class ReservationCreateSerializer(serializers.Serializer):
    start = serializers.DateField()
    end = serializers.DateField()
    rooms = serializers.ListField(required=False, child=serializers.IntegerField(max_value=1000), max_length=5)
    occupation = serializers.IntegerField()

    def validate(self, data):
        start = data.get("start")
        end = data.get("end")
        if start > end:
            raise serializers.ValidationError('End date is not after start date.')
        rooms = data.get("rooms")
        hotel = 0
        total = 0
        max = 0
        reserved_rooms = 0
        rooms_reserved = []
        # room_existence = Room.objects.filter(id__in=rooms).count()
        # if not room_existence > 0:
        #     raise serializers.ValidationError('Room does not exist')
        for room in rooms:
            room_object = Room.objects.filter(pk=room).first()
            room_existence = Room.objects.filter(id=room).count()
            if not room_existence > 0:
                raise serializers.ValidationError('Room id '+ str(room) + ' does not exists.')
            max = room_object.max_occupation + max
            if hotel == 0:
                hotel = room_object.hotel_id
            elif not room_object.hotel_id == hotel:
                raise serializers.ValidationError('Rooms are not in same hotel.')
            total = room_object.price + total
            reservations = Reservation.objects.all()
            for reservation in reservations:
                if start >= reservation.start:
                    if start <= reservation.end:
                        r = RoomReservation.objects.filter(reservation_id=reservation.pk)
                        for rr in r:
                            if rr.room.id == room:
                                reserved_rooms = reserved_rooms + 1
                                rooms_reserved.append(room)
                elif end >= reservation.start:
                    if end <= reservation.end:
                        r = RoomReservation.objects.filter(reservation_id=reservation.pk)
                        for rr in r:
                            if rr.room.id == room:
                                reserved_rooms = reserved_rooms + 1
                                rooms_reserved.append(room)
                elif reservation.start > start:
                    if reservation.start < end:
                        r = RoomReservation.objects.filter(reservation_id=reservation.pk)
                        for rr in r:
                            if rr.room.id == room:
                                reserved_rooms = reserved_rooms + 1
                                rooms_reserved.append(room)
                elif reservation.end > start:
                    if reservation.end < end:
                        r = RoomReservation.objects.filter(reservation_id=reservation.pk)
                        for rr in r:
                            if rr.room.id == room:
                                rooms_reserved.append(room)
                                reserved_rooms = reserved_rooms + 1
        if not reserved_rooms == 0:
            raise serializers.ValidationError(
                '(' + str(reserved_rooms) + ' Coincidences) Rooms ' + str(rooms_reserved) + ' are rented.')
        if max < data.get("occupation"):
            raise serializers.ValidationError('Too many people to stay in the rooms.')
        data["total"] = total
        return data

    def validate_end(self, value):
        now = datetime.date.today()
        end = value
        if end < now:
            raise serializers.ValidationError('This is not the future.')
        return value

    def validate_start(self, value):
        now = datetime.date.today()
        start = value
        if start < now:
            raise serializers.ValidationError('Start date has passed out.')
        return value

    def save(self):
        start = self.validated_data.get("start")
        end = self.validated_data.get("end")
        rooms = self.validated_data.get("rooms")
        total = self.validated_data.get("total")
        occupation = self.validated_data.get("occupation")
        instances = Reservation(start=start, end=end, total=total)
        instances.save()
        for room in rooms:
            relation = RoomReservation(room_id=room, reservation_id=instances.id, occupation=occupation)
            relation.save()
        return True
