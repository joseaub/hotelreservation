from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from .models import State, PhoneNumber, Hotel
import datetime


class PhoneNumberInitSerializer(serializers.Serializer):
    phone_number = serializers.CharField()


class HotelCreateSerializer(serializers.Serializer):
    address = serializers.CharField()
    name = serializers.CharField()
    state = serializers.IntegerField()

    def validate_address(self, value):
        address = value
        address_in_db = Hotel.objects.filter(address=address)
        if address_in_db.exists():
            raise serializers.ValidationError('This address is already taken')
        return value

    def validate_name(self, value):
        return value

    def validate_state(self, value):
        state = value
        state_in_db = State.objects.filter(pk=state)
        if not state_in_db.exists():
            raise serializers.ValidationError('Unknown state')
        return value

    def save(self):
        state = self.validated_data.get("state")
        name = self.validated_data.get("name")
        address = self.validated_data.get("address")
        state_in_db = State.objects.filter(pk=state).first()
        instances = Hotel(address=address, state_id=state_in_db.pk, name=name)
        instances.save()
        return True


class HotelSerializer(serializers.Serializer):
    name = serializers.CharField()
    address = serializers.CharField()
    state = serializers.CharField()
    phone_list = serializers.SerializerMethodField()

    def get_phone_list(self, object):
        phone_list = PhoneNumber.objects.filter(hotel_id=object.pk)
        return PhoneNumberInitSerializer(phone_list, many=True).data


class HotelPhonesSerializer(serializers.Serializer):
    name = serializers.CharField()
    phone_list = serializers.SerializerMethodField()
    phone_quantity = serializers.SerializerMethodField()

    def get_phone_list(self, object):
        phone_list = PhoneNumber.objects.filter(hotel_id=object.pk)
        return PhoneNumberInitSerializer(phone_list, many=True).data

    def get_phone_quantity(self, object):
        phone_quantity = PhoneNumber.objects.filter(hotel_id=object.pk).count()
        return phone_quantity


class PhoneNumberSerializer(serializers.Serializer):
    phone_number = serializers.CharField()


class PhoneNumberCreateSerializer(serializers.Serializer):
    phone_number = serializers.CharField()
    hotel = serializers.IntegerField()

    def validate_hotel(self, value):
        hotel = value
        hotel_in_db = Hotel.objects.filter(pk=hotel)
        if not hotel_in_db.exists():
            raise serializers.ValidationError('Theres no hotel')
        phone_quantity = PhoneNumber.objects.filter(hotel_id=hotel).count()
        if phone_quantity > 2:
            raise serializers.ValidationError('Too many phone numbers for an hotel')
        return value

    def validate_phone_number(self, value):
        phone_number = value
        phone_number_in_db = PhoneNumber.objects.filter(phoneNumber = phone_number)
        if phone_number_in_db.exists():
            raise serializers.ValidationError("Phone number already exists")
        return value

    def save(self):
        hotel = self.validated_data.get("hotel")
        phone_number = self.validated_data.get("phoneNumber")
        hotel_in_db = Hotel.objects.filter(pk=hotel).first()
        instances = PhoneNumber(phoneNumber=phone_number, hotel_id=hotel_in_db.pk)
        instances.save()
        return True


class StateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = State
        fields = ('id', 'name')
        hotels = serializers.PrimaryKeyRelatedField(queryset=Hotel.objects.all())


class StateCreateSerializer(serializers.Serializer):
    name = serializers.CharField()

    def validate_name(self, value):
        name = value
        name_in_db = State.objects.filter(name=name)
        if name_in_db.exists():
            raise serializers.ValidationError('That place is alredy registered')
        return value

    def saveState(self):
        name = self.validated_data.get("name")
        instances = State(name=name)
        instances.save()
        return True
