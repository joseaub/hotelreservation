from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics, status, mixins
from .serializers import (PhoneNumberSerializer, PhoneNumberCreateSerializer,
    HotelSerializer, HotelCreateSerializer, HotelPhonesSerializer,
    StateSerializer, StateCreateSerializer)
from .models import State, PhoneNumber, Hotel
from rest_framework.response import Response
from rest_framework.request import Request


class StateViewSet(viewsets.ModelViewSet):
    serializer_class = StateSerializer

    def get_queryset(self):
        return State.objects.all()

    def create(self, request):
        valid_serializer = StateCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.saveState()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StateList(generics.ListCreateAPIView):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #                       IsOwnerOrReadOnly,)

    #
    # def get(self, request, *args, **kwargs):
    #     return self.list(request, *args, **kwargs)
    #
    # def post(self, request, *args, **kwargs):
    #     return self.create(request, *args, **kwargs)


class PhoneNumberViewSet(viewsets.ModelViewSet):
    serializer_class = PhoneNumberSerializer

    def get_queryset(self):
        return PhoneNumber.objects.all()

    def list(self, request):
        # phones = PhoneNumber.objects.all()
        hotels = Hotel.objects.all()
        response = dict()
        response['status'] = True
        response['code'] = 350
        # response['data'] = PhoneNumberSerializer(phones, many=True).data
        response['data'] = HotelPhonesSerializer(hotels, many=True).data
        return Response(response)

    def create(self, request):
        valid_serializer = PhoneNumberCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.save()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HotelViewSet(viewsets.ModelViewSet):
    serializer_class = HotelSerializer

    def get_queryset(self):
        return Hotel.objects.all()

    def create(self, request):
        valid_serializer = HotelCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.save()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
