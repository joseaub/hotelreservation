from django.db import models
from django.utils import timezone
from datetime import date
from django.conf import settings
from django.contrib.auth.models import User


class State(models.Model):
    name = models.CharField(max_length=24, blank=False, null=False)

    # def __str__(self):
    #     return self.name

    class Meta:
        ordering = ('name',)


class Hotel(models.Model):
    name = models.CharField(max_length=24, blank=False, null=False)
    address = models.CharField(max_length=255, blank=False, null=False, unique=True)
    rank = models.IntegerField(blank=True, null=True)
    state = models.ForeignKey('State', blank=False, null=True, on_delete=models.SET_NULL)

    # def __str__(self):
    #     return self.name

    class Meta:
        ordering = ('rank',)


class PhoneNumber(models.Model):
    hotel = models.ForeignKey('Hotel', blank=True, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=13, blank=False, null=False)

    # def __str__(self):
    #     return self.phone_number
