"""hotelReservation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from .hotel import views as hotel_views
from .room import views as room_views
from .reservation import views as reservation_views
from .customer import views as customer_views
router = routers.DefaultRouter()
router.register(r'users', customer_views.UserViewSet, basename='user')
router.register(r'groups', customer_views.GroupViewSet)
router.register(r'customers', customer_views.CustomerViewSet, basename='customer')
router.register(r'states', hotel_views.StateViewSet, basename='state')
# router.register(r'estados', views.StateList)
router.register(r'phoneNumbers', hotel_views.PhoneNumberViewSet, basename='phoneNumber')
router.register(r'hotels', hotel_views.HotelViewSet, basename='hotel')
router.register(r'rooms', room_views.RoomViewSet, basename='room')
router.register(r'reservations', reservation_views.ReservationViewSet, basename='reservation')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # path('estados/', views.StateList.as_view()),
]
